#ifndef CHOOSEROKUDIALOG_H
#define CHOOSEROKUDIALOG_H

#include "roku.h"

#include <QDialog>
#include <QString>
#include <QUdpSocket>
#include <QVector>

namespace Ui
{
class ChooseRokuDialog;
}

class ChooseRokuDialog : public QDialog
{
  Q_OBJECT

 public:
  explicit ChooseRokuDialog(QWidget* parent = 0);
  ~ChooseRokuDialog();
  void set_roku_list(QList<QString> rokus);
  QString get_chosen_roku();

 private slots:
  void on_buttonBox_accepted();
  void on_rokuList_clicked(const QModelIndex& index);
  void on_rokuList_doubleClicked(const QModelIndex& index);

 private:
  Ui::ChooseRokuDialog* ui;

  QList<QString> rokus;
  QString currentRoku;
};

#endif  // CHOOSEROKUDIALOG_H
