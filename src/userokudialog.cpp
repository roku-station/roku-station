#include "userokudialog.h"
#include "rokuchannel.h"
#include "ui_userokudialog.h"

#include <QCoreApplication>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QImage>
#include <QKeyEvent>
#include <QLabel>
#include <QListWidgetItem>

UseRokuDialog::UseRokuDialog(QWidget* parent)
    : QDialog(parent), ui(new Ui::UseRokuDialog)
{
  ui->setupUi(this);
  this->setWindowTitle("Roku Station");
  qApp->installEventFilter(this);
}

UseRokuDialog::~UseRokuDialog() { delete ui; }

void UseRokuDialog::set_roku(Roku* roku)
{
  this->roku = roku;
  this->setWindowTitle("Roku Station | " + this->roku->get_ip());

  foreach (RokuChannel channel, this->roku->get_channels())
  {
    this->ui->channelList->addItem(new QListWidgetItem(channel.get_name()));
  }
}

void UseRokuDialog::on_channelList_doubleClicked(const QModelIndex& index)
{
  RokuChannel channel = this->roku->get_channels().at(index.row());

  if (this->roku->launch(channel.get_name()))
  {
    this->setWindowTitle("Roku Station | " + channel.get_name());

    this->ui->channelList->hide();

    // Create scene for displaying the image and load from channel
    QGraphicsScene* sc = new QGraphicsScene();
    sc->setBackgroundBrush(Qt::black);
    QPixmap pxmp;
    pxmp.loadFromData(channel.get_icon());
    sc->addPixmap(pxmp);

    // Create view to display scene and add to UI
    QGraphicsView* gv = new QGraphicsView(sc);
    this->ui->gridLayout->addWidget(gv);
    gv->show();
    // gv->fitInView(sc->sceneRect(), Qt::KeepAspectRatio);
  }
  else
  {
    this->setWindowTitle("Roku Station | ERROR");
    qDebug() << "Error launching the channel";
  }
}

void UseRokuDialog::keyPressEvent(QKeyEvent* ev)
{
  qDebug() << "You typed: " << QString(ev->key());
}

bool UseRokuDialog::eventFilter(QObject* watched, QEvent* event)
{
  if (event->type() == QEvent::KeyPress)
  {
    QKeyEvent* kev = static_cast<QKeyEvent*>(event);

    int k = kev->key();
    if (k == Qt::Key_Up)
    {
      this->roku->up();
    }
    else if (k == Qt::Key_Down)
    {
      this->roku->down();
    }
    else if (k == Qt::Key_Left)
    {
      this->roku->left();
    }
    else if (k == Qt::Key_Right)
    {
      this->roku->right();
    }
    else if (k == Qt::Key_Return)
    {
      this->roku->select();
    }
    else if (k == Qt::Key_Escape)
    {
      this->roku->back();
    }
    else if (k == Qt::Key_Backspace || k == Qt::Key_Delete)
    {
      this->roku->backspace();
    }
    else if (kev->modifiers() == Qt::ControlModifier)
    {
      if (k == Qt::Key_Space)
      {
        this->roku->pause();
      }
      else if (k == Qt::Key_Left)
      {
        this->roku->rev();
      }
      else if (k == Qt::Key_Right)
      {
        this->roku->fwd();
      }
      else if (k == Qt::Key_H)
      {
        this->roku->home();
      }
    }
    else
    {
      if (kev->text() != "")
      {
        this->roku->character(kev->text());
      }
    }

    event->accept();
    return true;
  }

  return false;
}
