# Roku Station: Roku for the desktop

## Build
```
mkdir build && cd build
qmake -o Makefile ../src/roku-station.pro -spec linux-g++
make
```

## Run
```
./roku-station
```
